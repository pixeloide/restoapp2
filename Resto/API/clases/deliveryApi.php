<?php

require_once 'AccesoDatos.php';
require_once 'pedido.php';
require_once 'pedidoApi.php';
require_once 'clienteVisitaApi.php';
require_once 'mesa.php';

class deliveryApi
{

   public static function estadoDeliveryBoy($req,$res){
      $rta=new stdClass();
      $rta->pendientes=pedido::traerPedidosParaDeliveryPorEstado(9);
    
      $pedidosYendo = pedido::traerPedidosParaDeliveryPorEstado(8);
      $pedidosListos=pedido::traerPedidosParaDeliveryPorEstado(3);
      $rta->listos= array_merge($pedidosListos,$pedidosYendo);
      
      return  $res->withJson($rta, 200);
   }

   public static function pedidosPendientesDeAprobacion($req, $res)
   {
      $pedidos = pedido::traerPedidosParaDeliveryPorEstado(9);
      return  $res->withJson($pedidos, 200);
   }
   public static function empezarViaje($req, $res)
   {
      $token = $req->getParsedBody()["token"];
      $id_pedido = $req->getParsedBody()["id_pedido"];
      $payload = AutentificadorJWT::ObtenerData($token);
      $idDeliveryBoy = $payload->id_empleado;

      pedido::cambiarEstadoPedido($id_pedido,8); //en viaje
      $sql="UPDATE `pedidos_delivery` SET `id_empleado` = '".$idDeliveryBoy."' WHERE `pedidos_delivery`.`id_pedido` = ".$id_pedido;
      $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();
      $consulta = $objetoAccesoDato->RetornarConsulta($sql);
      $consulta->execute();
      

      return self::estadoDeliveryBoy($req,$res);
   }

   
   public static function clienteInformaQRecibioPedido($request, $response, $args){
      $id=$request->getParsedBody()["id_pedido"];
     $rta= pedido::cambiarEstadoPedido($id,10);
     return $response->withJson($rta, 200);
   }

   public static function pedidosListosParaDelivery($request, $response, $args)
   {

      $pedidos = pedido::traerPedidosParaDeliveryPorEstado(3);
      $pedidosYendo = pedido::traerPedidosParaDeliveryPorEstado(8);
      $pedidos2= array_merge($pedidos,$pedidosYendo);
      return $response->withJson($pedidos2, 200);
   }

   public function AceptarUnDelivery($request, $response, $args)
   {
      $vId = $args['id']; 
      $rta = pedido::cambiarEstadoPedido($vId,1);
      //esto devuelve la lista de pedidos pendientes, lo mismo q el ESTADO del deliveryboy
      $pedidos = pedido::traerPedidosParaDeliveryPorEstado(9);
      return  $response->withJson($pedidos, 200);
   }

   public function rechazarDelivery($request, $response, $args)
   {
      $vId = $args['id']; 
      $rta = pedido::cambiarEstadoPedido($vId,5);
      //esto devuelve la lista de pedidos pendientes, lo mismo q el ESTADO del deliveryboy
      $pedidos = pedido::traerPedidosParaDeliveryPorEstado(9);
      return  $response->withJson($pedidos, 200);
   }

   public function CargarUnDelivery($request, $response, $args)
   {
      //esta es la func q se usa en el index.php
      $obj = new stdclass();
      $obj->respuesta = "";
      $obj->itsOk = false;

      $vPedido = new pedido();
      $vHora = new DateTime();
      $vector = $request->getParsedBody();



      $vPedido->id_mesa = -8;
      $vPedido->estado_pedido = 9;
      $vPedido->id_cliente = $vector['id_cliente'];


      $lat = $vector['latitud'];
      $lng = $vector['longitud'];
      $direccion = $vector['direccion'];

      $productos = $vector['productos'];
      $cantidades = $vector['cantidades'];

       

      if (count($productos) != count($cantidades)) {
         $obj->respuesta = "Las cantidades y productos deben coincidir";
      } else {
        


            try {

               $vPedido->fecha_alta = date_format($vHora, "Y/m/d H:i:s");
               $vPedido->hora_estimada = pedidoApi::TraerMayorTiempo($productos);
         
               

               $una_cliente_visita = new ClienteVisita();

               $una_cliente_visita->id_cliente = $vPedido->id_cliente;
               $una_cliente_visita->id_mesa = -8;
               $una_cliente_visita->fecha = date("Y-m-d H:i:s");
               $una_cliente_visita->comensales = -8;
               $una_cliente_visita->mozo = -8;
               $vPedido->id_cliente_visita = $una_cliente_visita->InsertarClienteVisita();


               $vPedido->id = $vPedido->InsertarPedidoDelivery();
               $idDelivery =  $vPedido->InsertarDelivery($vPedido->id, $vPedido->id_cliente, $lat, $lng, $direccion);
               $vPedido->CargarPedidosDetalle($productos, $cantidades, $vPedido->id);

               $obj->respuesta = "Se cargo el pedido: " . $vPedido->id;
               $rta["id_pedido"] = $vPedido->id;
               $rta["id_delivery"] = $idDelivery;
               $obj->itsOk = true;


               //Comento todo lo vinculado a los roles de los empleados vinculados al pedido
               //EXTRAIGO LOS ROLES DE EMPLEADOS Q ESTE PEDIDO TIENE
               // $ids_roles_q_este_pedido_involucra = [];
               // for ($j = 0; $j < count($productos); $j++) {
               //     $p = producto::TraerUno($productos[$j]);
               //     array_push($ids_roles_q_este_pedido_involucra, $p[0]->id_cocina);
               // }
               // $ids_roles_q_este_pedido_involucra = array_values($ids_roles_q_este_pedido_involucra);

               // // var_dump($ids_roles_q_este_pedido_involucra);die();
               // //MANDO NOTIFICACIONES POR SECTOR
               // for ($j = 0; $j < count($ids_roles_q_este_pedido_involucra); $j++) {
               //     //  echo $ids_roles_q_este_pedido_involucra[$j]."--";
               //     try {
               //         oneSignal::mandarPushARolDeUsuario($ids_roles_q_este_pedido_involucra[$j], "Nuevo pedido! " . $vPedido->id);
               //     } catch (Exception $e) { }
               // }
            } catch (Exception $e) {
               return $e->getMessage();
            }
        
      }
      $newResponse = $response->withJson($obj, 200);
      return $newResponse;
   }

   public static function traerPedidosDeliveryPorIdCliente($id_cliente)
   {
       $consulta = "SELECT pedidos.*, clientes.nombre_completo, pedidos_delivery.direccion, pedidos_delivery.lat, pedidos_delivery.lng
       FROM `pedidos`, `clientes`, `pedidos_delivery`
       WHERE pedidos_delivery.id_pedido = pedidos.id
       and clientes.id_cliente=pedidos_delivery.id_cliente
       and pedidos.id_mesa=-8 
       and pedidos_delivery.id_cliente=".$id_cliente;

       return AccesoDatos::ConsultaDatosAsociados($consulta);
   }
   
}
