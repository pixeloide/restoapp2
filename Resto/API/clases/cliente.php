<?php

require_once 'AccesoDatos.php';

class cliente
{
    public $id_cliente;
    public $nombre_completo;
    public $dni;
    public $email;
    public $foto;
    public $habilitado = 0;
    
    public function __construct() {}

        public function Insertar()
        {                 
            
          

            $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso(); 
            $sql="INSERT into clientes( nombre_completo, dni, email, foto, habilitado) values('$this->nombre_completo', '$this->dni','$this->email', '', '$this->habilitado')";
           


            $consulta =$objetoAccesoDato->RetornarConsulta($sql);
            

            $consulta->execute();		

            $id_cliente=$objetoAccesoDato->RetornarUltimoIdInsertado();


           

            if (isset($this->foto) && !empty($this->foto)) {
                //$emp->foto = $vector['foto']; 
    
                 //FOTOS		
                 $carpeta="img/clientes/".$this->id_cliente."/";
                 if(!is_dir($carpeta)) mkdir($carpeta);
                 $foto_emple=$carpeta.$this->id_cliente."-foto_cliente-".date('Y-m-d--H.i.s').".png";
                 
               

                 $img = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $this->foto));
    
                 

                file_put_contents($foto_emple, $img);
                $this->foto = $foto_emple; 
    
    
            } else {  
                //IMAGEN PLACEHOLDER        
                $this->foto = "img/user.jpg";
            }


           $sql="    update clientes set foto ='$this->foto'    WHERE id_cliente ='$id_cliente'";

        
           
            $consulta =$objetoAccesoDato->RetornarConsulta($sql);
              $consulta->execute();   
              return $id_cliente;

        }

        public static function TraerTodos()
        {
            $consulta = "SELECT * FROM `clientes`";
            return AccesoDatos::ConsultaClase($consulta,"cliente");
        }

        public static function email2Cliente($email)
        {             
            $consulta = "SELECT * FROM `clientes` WHERE  `email` = '$email'";
            return AccesoDatos::ConsultaClase($consulta,"cliente");
        }

        public static function dni2Cliente($dni)
        {             
            $consulta = "SELECT * FROM `clientes` WHERE  `dni` = '$dni'";
            return AccesoDatos::ConsultaClase($consulta,"cliente");
        }

        public static function TraerUno($vIdCliente)
        {             
            $consulta = "SELECT * FROM `clientes` WHERE  `id_cliente` = '$vIdCliente'";
            return AccesoDatos::ConsultaClase($consulta,"cliente");
        }

        public function Modificar()
        {
            $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso(); 
            $consulta =$objetoAccesoDato->RetornarConsulta("
                update clientes set
                nombre_completo ='$this->nombre_completo'
                WHERE id_cliente ='$this->id_cliente'");
            return $consulta->execute();    
        }

        public function BorrarUno()
        {
            $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso(); 
            $consulta =$objetoAccesoDato->RetornarConsulta("
            delete 
            from clientes 				
            WHERE id_cliente =:id_cliente");	
            $consulta->bindValue(':id_cliente',$this->id_cliente, PDO::PARAM_INT);		
            $consulta->execute();
            return $consulta->rowCount();
        }

        public static function cantVisitasCliente($id) 
        {
            $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso(); 
            $consulta =$objetoAccesoDato->RetornarConsulta("select * from cliente_visita where id_cliente =".$id);	
           
            $consulta->execute();   
            
          
            return   $consulta->rowCount();
        }


        
   
}
