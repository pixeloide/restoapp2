var game 
var mainState
var Flappy = function() {
   
         game = new Phaser.Game(400, 710, Phaser.AUTO, "gameDiv");
        
         mainState = {

            preload: function() {
               // alert("preload boot") 
                if(!game.device.desktop) {
                    game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                    game.scale.setMinMax(game.width/2, game.height/2, game.width, game.height);
                }
             
                
                game.scale.pageAlignHorizontally = true;
                game.scale.pageAlignVertically = true;

                game.stage.backgroundColor = '#71c5cf';

                game.load.image('bird', 'partes/flappy-juego/assets/bird.png');  
                game.load.image('pipe', 'partes/flappy-juego/assets/pipe.png'); 

                // Load the jump sound
                game.load.audio('jump', 'partes/flappy-juego/assets/jump.wav'); 
            },

            create: function() { 
                game.physics.startSystem(Phaser.Physics.ARCADE);
                this.espacioTemporalColumnas=1800
                this.pipes = game.add.group();
                this.timer = game.time.events.loop( this.espacioTemporalColumnas, this.addRowOfPipes, this);           

                this.bird = game.add.sprite(100, 245, 'bird');
                game.physics.arcade.enable(this.bird);
                this.bird.body.gravity.y = 1000; 

                // New anchor position
                this.bird.anchor.setTo(-0.2, 0.5); 
         
                var spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
                spaceKey.onDown.add(this.jump, this); 
                game.input.onDown.add(this.jump, this);

                this.score = 0;
                this.labelScore = game.add.text(20, 20, "0", { font: "30px Arial", fill: "#ffffff" });  

                // Add the jump sound
                this.jumpSound = game.add.audio('jump');
                this.jumpSound.volume = 0.2;

                // CN
              //  CNContentInterface.ready(this);
            },

            update: function() {
                if ( this.bird.y > game.world.height) this.restartGame(); 

                game.physics.arcade.overlap(this.bird, this.pipes, this.hitPipe, null, this); 
                    
                // Slowly rotate the bird downward, up to a certain point.
                if (this.bird.angle < 20)
                    this.bird.angle += 1;  
            },

            jump: function() {
                // If the bird is dead, he can't jump
                if (this.bird.alive == false)
                    return; 

                this.bird.body.velocity.y = -350;

                // Jump animation
                game.add.tween(this.bird).to({angle: -20}, 100).start();

                // Play sound
                this.jumpSound.play();
            },

            hitPipe: function() {
                // If the bird has already hit a pipe, we have nothing to do
                if (this.bird.alive == false)
                    return;
                    
                // Set the alive property of the bird to false
                this.bird.alive = false;

                // Prevent new pipes from appearing
                game.time.events.remove(this.timer);
            
                // Go through all the pipes, and stop their movement
                this.pipes.forEach(function(p){
                    p.body.velocity.x = 0;
                }, this);
            },

            restartGame: function() {
                if(this.vidas==undefined) this.vidas=1;
                this.vidas--
                if(this.vidas==0) alert("Perdiste, tenes otra oportunidad, pero presta más atención");
                else{
                    console.log("!!!! perdiste");
                    sacarFlappy();
                    return;
                    }
                game.state.start('main');
            },

            addOnePipe: function(x, y) {
              
                var pipe = game.add.sprite(x, y, 'pipe');
                this.pipes.add(pipe);
                game.physics.arcade.enable(pipe);

                pipe.body.velocity.x = -200;  
                pipe.checkWorldBounds = true;
                pipe.outOfBoundsKill = true;

               
            },

            addRowOfPipes: function() {
                var hole = Math.floor(Math.random()*8);
              
                
                for (var i = 0; i < 15; i++)
                    if (i != hole && i != hole +1 && i != hole +2) 
                        this.addOnePipe(400, i*60+10);   
            
                this.score += 1;
                if(this.score>=5){
                  
                    ganasteFlappy();
                }
                this.labelScore.text = this.score;  
            },
        };

        game.state.add('main', mainState);  
        game.state.start('main'); 
        return game;
    
  
};
/*
document.addEventListener("CNContentInterfaceLoaded", function() {
    new Flappy
});*/
